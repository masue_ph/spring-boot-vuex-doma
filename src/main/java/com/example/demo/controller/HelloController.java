package com.example.demo.controller;

import com.example.demo.domain.service.UserService;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class HelloController {

    @Autowired
    private UserService userService;

    // TODO どちらでもかけるみたい PostMappingは最近できたらしく、RequestMappingを結合したものらしい
    @PostMapping("api/hello")
    //@RequestMapping(value = "api/hello", method = RequestMethod.POST)
    HelloResponse hello(@RequestBody final HelloRequest request) {
        final String name = request.name;

        String userName = userService.getUserById(1).map(m -> m.name).orElse("");

        final String message = String.format("Hello, %s  UserName : %s", name, userName);
        return new HelloResponse(message);
    }

    // TODO レスポンスの返却の仕方は変更したいけど、下の書き方は今はやりなのか確認中・・・
    public static final class HelloRequest {
        public final String name;

        public HelloRequest(@JsonProperty("name") final String name) {
            this.name = Objects.requireNonNull(name);
        }
    }

    public static final class HelloResponse {
        public final String message;

        public HelloResponse(final String message) {
            this.message = Objects.requireNonNull(message);
        }
    }
}
