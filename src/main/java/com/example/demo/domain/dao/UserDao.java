package com.example.demo.domain.dao;

import com.example.demo.domain.model.User;
import org.seasar.doma.Dao;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;

import java.util.List;
import java.util.Optional;

/**
 * Created by takanoma on 2017/11/17.
 */
@Dao
@ConfigAutowireable
public interface UserDao {

    @Select
    List<User> selectAll();

    @Select
    Optional<User> selectById(int id);
}
