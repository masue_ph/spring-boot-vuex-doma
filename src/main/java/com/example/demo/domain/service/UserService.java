package com.example.demo.domain.service;

import com.example.demo.domain.dao.UserDao;
import com.example.demo.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by takanoma on 2017/11/17.
 */
@Service
public class UserService extends BaseService {

    @Autowired
    UserDao userDao;

    public List<User> getUsers() {
        return userDao.selectAll();
    }

    public Optional<User> getUserById(int id) {
        return userDao.selectById(id);
    }
}
