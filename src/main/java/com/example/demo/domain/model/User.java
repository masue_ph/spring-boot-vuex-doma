package com.example.demo.domain.model;

import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * Created by takanoma on 2017/11/17.
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    /** id */
    public Integer id;

    /**
     * 名前
     */
    public String name;
}
